package com.juventietis.locationtracker

import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.io.BufferedReader
import java.io.StringReader
import java.io.StringWriter

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
class UnitTest {
    @Test
    fun convertingToGpxWorks(){
        val stringBufferedReader = BufferedReader(StringReader("2018-06-04T00:00:01,0.0,0.0,100\n2018-06-04T01:00:01,100.0,50.0,100\n"))
        val stringWriter = StringWriter()
        LocationTracker.convertToGpx(stringBufferedReader, stringWriter)
        val output = stringWriter.toString()
        val expectedString = "<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><gpx version=\"1.1\"><name>My Locations</name><trk><name>Waypoint</name><number>1</number><trkseg><trkpt lat=\"0.0\" lon=\"0.0\"><ele>100</ele><time>2018-06-04T00:00:01Z</time></trkpt><trkpt lat=\"100.0\" lon=\"50.0\"><ele>100</ele><time>2018-06-04T01:00:01Z</time></trkpt></trkseg></trk></gpx>"
        assertEquals(expectedString, output)
    }
}
