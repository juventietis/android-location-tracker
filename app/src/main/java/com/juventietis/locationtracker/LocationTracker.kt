package com.juventietis.locationtracker

import android.Manifest
import android.app.Activity
import android.content.*
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.os.*
import android.support.v4.content.LocalBroadcastManager
import android.util.Xml
import org.xmlpull.v1.XmlSerializer
import java.io.*
import android.content.Intent
import android.net.Uri


/**
 * Main application class for tracking location
 */
class LocationTracker : Activity() {
    companion object {
        private val INITIAL_PERMS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        private const val INITIAL_REQUEST = 1337
        private const val WRITE_REQUEST_CODE = 42

        /**
         * Read input from reader, convert it to GPX format and writes out to outputWriter
         * Expects input to be in format "datetime,lat,long,elevation\n"
         */
        fun convertToGpx(inputReader: BufferedReader, outputWriter: Writer){
            val serializer: XmlSerializer = Xml.newSerializer()
            serializer.setOutput(outputWriter)
            serializer.startDocument("UTF-8", true)
            serializer.startTag("","gpx")
            serializer.attribute("", "version", "1.1")
            serializer.startTag("", "name")
            serializer.text("My Locations")
            serializer.endTag("", "name")
            serializer.startTag("", "trk")
            serializer.startTag("", "name")
            serializer.text("Waypoint")
            serializer.endTag("", "name")

            serializer.startTag("", "number")
            serializer.text("1")
            serializer.endTag("", "number")
            serializer.startTag("","trkseg")

            for (row in inputReader.lineSequence()){
                val columns = row.split(",")
                serializer.startTag("", "trkpt")
                serializer.attribute("", "lat", columns[1])
                serializer.attribute("", "lon", columns[2])
                serializer.startTag("", "ele")
                serializer.text(columns[3])
                serializer.endTag("", "ele")
                serializer.startTag("", "time")
                serializer.text(columns[0]+"Z")
                serializer.endTag("", "time")
                serializer.endTag("", "trkpt")
            }
            serializer.endTag("", "trkseg")
            serializer.endTag("", "trk")
            serializer.endTag("", "gpx")
            serializer.endDocument()
        }
    }

    /** Messenger for communicating with the service.  */
    var mService: Messenger? = null

    /** Flag indicating whether we have called bind on the service.  */
    var mBound: Boolean = false

    /**
     * Class for interacting with the main interface of the service.
     */
    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = Messenger(service)
            mBound = true
            getTimestampOfLastStoredLocation()
        }

        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null
            mBound = false
        }
    }

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val message = intent.getStringExtra("message")
            Log.d("receiver", "Got message: $message")
            val myText = findViewById<TextView>(R.id.is_service_running)
            myText.text = message
        }
    }

    /**
     * Send a message to service, asking to store last known location now
     * Overrides the min wait time.
     */
    fun storeLocationNow() {
        if (!mBound) return
        // Create and send a message to the service, using a supported 'what' value
        val msg = Message.obtain(null, LocationTrackerService.STORE_LOCATION, 0, 0)
        try {
            mService?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    /**
     * Sends a message to the service requesting the timestamp of when was the last location stored.
     * The service then responds with a broadcast.
     */
    fun getTimestampOfLastStoredLocation() {
        if (!mBound) return
        // Create and send a message to the service, using a supported 'what' value
        val msg = Message.obtain(null, LocationTrackerService.GET_LOCATION_LAST_STORED, 0, 0)
        try {
            mService?.send(msg)
            Log.i("LocationTracker", "Send a request for last location stored")
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    override fun onStart() {
        super.onStart()
        // Bind to the service
        bindService(Intent(this, LocationTrackerService::class.java), mConnection,
                Context.BIND_AUTO_CREATE)
    }


    /**
     * Called on creating the app
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_tracker)

        requestPermissions(INITIAL_PERMS, INITIAL_REQUEST)

        val intent = Intent(this, LocationTrackerService::class.java)

        val closeButton: Button = this.findViewById<View>(R.id.close) as Button
        closeButton.setOnClickListener { stopBackgroudService(intent) }

        val startButton: Button = this.findViewById<View>(R.id.start) as Button
        startButton.setOnClickListener { startBackgroundService(intent) }

        val storeLocationButton: Button = this.findViewById<View>(R.id.store_location) as Button
        storeLocationButton.setOnClickListener { storeLocationNow() }

        val convertToGpxButton: Button = this.findViewById<View>(R.id.convert_to_gpx) as Button
        convertToGpxButton.setOnClickListener { writeGpxToFile() }

        val chooseFileButton: Button = this.findViewById<View>(R.id.choose_file) as Button
        chooseFileButton.setOnClickListener { fileChooser() }


        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter("new-location-stored"))

        startService(intent)
    }

    override fun onStop() {
        super.onStop()
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection)
            mBound = false
        }
    }

    private fun fileChooser(fileName: String = "gpsFile.txt") {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TITLE, fileName)
        startActivityForResult(intent, WRITE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WRITE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
                with(sharedPref.edit()) {
                    putString(getString(R.string.output_file_path), data?.data?.path)
                    apply()
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Unbinds and kills the background service.
     */
    private fun stopBackgroudService(intent: Intent){
        Log.i("LocationTracker", "Stopping Background Service")
        if(mBound){
            unbindService(mConnection)
            mBound = false
        }
        stopService(intent)
    }


    /**
     * Start the background service again if it was killed.
     * And also binds it.
     */
    private fun startBackgroundService(intent: Intent){
        Log.i("LocationTracker", "Starting Background Service")
        startService(intent)
        // Bind to the service
        bindService(Intent(this, LocationTrackerService::class.java), mConnection,
                Context.BIND_AUTO_CREATE)
    }

    /**
     * Reads the stored .txt file and write output to .gpx file in same directory.
     */
    private fun writeGpxToFile(){
        val file = File(LocationTrackerService.getGpsStorageDir("gps"), "gpsFile.gpx")
        val outputFileWriter = FileWriter(file)
        val gpsFile = File(LocationTrackerService.getGpsStorageDir("gps"), "gpsFile.txt")
        val reader = gpsFile.bufferedReader()
        convertToGpx(reader, outputFileWriter)
        outputFileWriter.close()
    }
}
