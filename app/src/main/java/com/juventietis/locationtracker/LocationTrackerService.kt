package com.juventietis.locationtracker

import android.Manifest
import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.util.Log
import android.widget.Toast
import java.io.File
import java.io.FileWriter
import java.util.*
import android.support.v4.app.NotificationCompat
import android.app.PendingIntent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import java.util.concurrent.atomic.AtomicInteger
import java.text.SimpleDateFormat


object NotificationID {
    private val c = AtomicInteger(0)
    val id: Int
        get() = c.incrementAndGet()
}

/**
 * Service for tracking location in the background and writing it to file
 */
class LocationTrackerService : Service() {
    companion object {
        val STORE_LOCATION = 1
        val GET_LOCATION_LAST_STORED = 2
        private val FOREGROUND_ID = 7658654


        fun getGpsStorageDir(fileName: String): File? {
            val file = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), fileName)
            if (!file.mkdirs()) {
                Log.e("LocationTrackerService", "Directory not created")
            }
            return file
        }
    }

    fun getFilePathFromSettings(){
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val outputPath = preferences.getString(getString(R.string.output_file_path), "empty")
        val filePath = File(outputPath)
    }

    private val gpsDir = getGpsStorageDir("gps")

    private var locationLastStored : String = "Not stored yet!"

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    val mMessenger = Messenger(IncomingHandler())

    var notificationManager : NotificationManagerCompat? = null

    /** Command to the service to display a message  */


    /**
     * Handler of incoming messages from clients.
     */
    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                STORE_LOCATION -> {Toast.makeText(applicationContext, "Storing location!", Toast.LENGTH_SHORT).show(); storeLocationNow()}
                GET_LOCATION_LAST_STORED -> sendLocationLastStored(locationLastStored)
                else -> super.handleMessage(msg)
            }
        }
    }

    private fun sendLocationLastStored(lastTimestamp: String? = null) {
        val realLastTimestamp = lastTimestamp ?: prepareTimestamp()
        val intent = Intent("new-location-stored")
        intent.putExtra("message", "Last location stored at: $realLastTimestamp")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onBind(intent: Intent): IBinder? {
        Toast.makeText(applicationContext, "binding", Toast.LENGTH_SHORT).show()
        return mMessenger.binder

    }

    override fun onCreate() {
        Log.e("LocationTrackerService", "STARTING SERVICE")
        // Acquire a reference to the system Location Manager
        val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        // Define a listener that responds to location updates
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                persistLocation(location)
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

            override fun onProviderEnabled(provider: String) {}

            override fun onProviderDisabled(provider: String) {}
        }

        notificationManager = NotificationManagerCompat.from(this)
        // Register the listener with the Location Manager to receive location updates
        if(PackageManager.PERMISSION_GRANTED == checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)){
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 30 * 60 * 1000L, 5.0f, locationListener)
        }
        startForeground(FOREGROUND_ID, buildForegroundNotification())
        super.onCreate()
    }

    override fun onDestroy() {
        Log.e("LocationTrackerService", "Service Getting Destroyed")
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.e("LocationTrackerService", "ON START COMMAND STARTING SERVICE")
        return START_STICKY
    }

    private fun buildForegroundNotification(): Notification {
        val b = NotificationCompat.Builder(this,"FOREGROUND_ID")
        // Create an explicit intent for an Activity in your app
        val intent = Intent(this, LocationTracker::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        b.setOngoing(true)
                .setContentTitle("LocationTrackingService")
                .setContentText("Running")
                .setSmallIcon(android.R.drawable.stat_notify_sync)
                .setTicker("LocationTrackingService")
                .setContentIntent(pendingIntent)
        return b.build()
    }

    private fun buildNotifactionForSavedLocation(): Notification {
        // Create an explicit intent for an Activity in your app
        val intent = Intent(this, LocationTracker::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val mBuilder = NotificationCompat.Builder(this, "FOREGROUND_ID")
                .setSmallIcon(android.R.drawable.stat_notify_sync)
                .setContentTitle("LocationService")
                .setContentText("Saved your location")
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
        return mBuilder.build()
    }

    private fun persistLocation(location: Location) {
        Log.e("LocationTrackerService", "Received new location")
        //notificationManager?.notify(NotificationID.id, buildNotifactionForSavedLocation())

        if(isExternalStorageWritable()){
            Log.e("LocationTrackerService", "WRITE_EXTERNAL_STROGE")
            val file = File(gpsDir, "gpsFile.txt")
            val fileWriter = FileWriter(file, true)
            Log.e("LocationTrackerService", file.absolutePath)
            try {
                val gpsStr = prepareOutput(location)
                Log.i("LocationTrackerService", "Logged location: $gpsStr")
                fileWriter.write(gpsStr)
                fileWriter.close()
            } catch (e: Exception) {
                Log.e("LocationTrackerService", e.stackTrace.toString())
            }

        }
    }

    private fun prepareOutput(location: Location): String {
        val sb = StringBuilder()
        return sb.append(prepareTimestamp())
                .append(",")
                .append(location.latitude.toString())
                .append(",")
                .append(location.longitude.toString())
                .append(",")
                .append(location.altitude.toString())
                .append("\n")
                .toString()
    }

    private fun prepareTimestamp(): String{
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        val date = calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        return sdf.format(date)
    }


    /* Checks if external storage is available for read and write */
    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }


    private fun storeLocationNow(){
        val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if(PackageManager.PERMISSION_GRANTED == checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            persistLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER))
        }
        sendLocationLastStored()
    }
}
